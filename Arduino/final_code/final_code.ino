/* Author: Eliza Yeao
 * 
 * Rev 5.0 June 8, 2022
 * 
 * This is the program that the Arduino NANO will execute when a bike rides over a pneumatic tube and triggers the driver alert, 
 * which is 100 LEDs on a sign, to start blinking.
 */


#include <SD.h>
#include <SPI.h>
#include <LowPower.h>
#include <TimerOne.h>

File myFile;

// sensor values 
int sensorPin = A0;                                    // analog input pin for sensor to be read by 10-bit ADC
int sensorValue = 0;                                   // actual digital value of sensor input 
float bikeCounter = 0;                                 // counter to keep track of number of bikes that passed
int bikeFlag = LOW;                                    // flag to keep track of whether a bike is detected or not
int high_threshold = 100;                              // highest digital input of sensor for bikes
int low_threshold = 50;                                // lowest digital input of sensor for bikes

// driver alert LED values
int driverLED = 9;                                     // digital output pin for the 100 LEDs driver alert using PWM
int blinkInterval = 500;                               // measures blinks/min for one cycle of driver alert LEDs
int maxBrightness = 255;                               // maximum brightness of LED 
int minBrightness = 0;                                 // minimum brightness of LED
int highBrightness = 255;                              // brightness of LED for high solar intensity 
int lowBrightness = 155;                               // brightness of LED for low solar intensity
int ledState = maxBrightness;                          // whether the LED is at max or min brightness
unsigned long previousBlinkMillis = 0;                 // keep track of previous time the driver alert LED was on or off 
int blinkCounter = 0;                                  // measures how many times to cycle through on/off of driver alert LED
int maxBlinkCount = 350;                               // the maximum amount of times we want to blink driver alert LEDs
int lowPowerBlink = 575;                               // the interval between blinks low power so we get 50 blinks/min
int highPowerBlink = 500;                              // the interval between blinks low power so we get 60 blinks/min

// battery values
unsigned long previousBatteryMillis = 0;               // keep track of previous time the battery data was collected
const unsigned long batteryInterval = 3600000UL;       // how often we want to collect battery data (every hour so 3600000UL)
int voltagePin = A1;                                   // this is the output of the voltage sensor that brings voltage down to <5V from battery
int solarVoltagePin = A2;                              // this is the output of the voltage sensor that brings voltage down to <5V from solar panel
int solarCurrentPin = A3;                              // this is the output of the current sensor
float dividerRatio = 5;                                // this is (R1+R2)/R2 where R1 = 22kOhm and R2 = 10kOhm 
float refVoltage = 5.00;                               // this is what we want to bring voltage down to
float voltageReading = 0;                              // converted digital battery reading to analog
float solarVoltageReading = 0;                         // converted digital solar reading to analog

// SD card values
const int chipSelect = 10;                             // CS for SPI 
const char bikeFile[] = "bike.txt";                    // file for storing bike data
const char batteryFile[] = "battery.txt";              // file for storing battery data

// error LED values
int errorPin = 8;                                      // digital output pin for error LED
int errorLEDState = LOW;                               // whether error LED is on or off
unsigned long previousErrorMillis = 0;                 // keep track of previous time the error LED was HIGH or LOW 
int errorInterval = 1000;                              // measures blinks/min for one cycle of error LEDs

// ISR values
volatile int sensorCount = 0;                          // keeps track of how many times the ISR was triggered
volatile int totalSensorValue = 0;                     // accumulation of sensor output values
int maxTrigger = 5;                                    // maximum times we want to trigger the ISR for each data collection
int avgSensorValue = 0;                                // average of sensor values (found by dividing totalSensorValue/maxTrigger)


void setup() {

  Serial.begin(9600);

  // set the driver alert LEDs to OUTPUT
  pinMode(driverLED, OUTPUT);

  // set the error LED to OUTPUT
  pinMode(errorPin, OUTPUT);

  // check if SD card initialized
  if (!SD.begin()) {

    BlinkErrorLED();
  
  }

  // timer interrupt setup so SensorISR runs every 0.05 seconds
  Timer1.initialize(50000);
  Timer1.attachInterrupt(SensorISR);

}


/* The SensorISR() ISR triggers every 0.05 seconds as initialized in the setup code. This is done using the TimerOne library provided by Arduino.
 *  
 *  When the ISR is triggered, sensor data is read using analogRead() and then a total sensor value and the number of times the ISR is triggered is kept track of, 
 *  so that in the main event loop, once the number of times the ISR is triggered has reached a predetermined value, the average of the total sensor value over
 *  how many times the ISR was triggered is calculated in order to find an average sensor reading.
 */

void SensorISR() {

  // collect sensor data
  sensorValue = analogRead(sensorPin);

  // do not want to count undershoot values from sensor noise
  if (sensorValue > 40) {

    // every time the ISR is triggered, add the measured sensor value to a total sensor value count
    totalSensorValue += sensorValue;

    // keep track of how many times the ISR is triggered
    sensorCount++;

  }
  
}


/* The main event loop() reads data from the sensor whenever a bike is detected, writes the bike count to a 
 * file using the WriteSD() function, and transmits an RF message to a second sign. It then calls the function BlinkDriverAlert() to begin blinking 
 * the driver alert LEDs for 3 minutes. 
 * 
 * While blinking, the system should still collect sensor input in case a bike passes while the driver alert is still blinking to reset the amount of 
 * time the driver alert blinks. 
 * 
 * If a bike is not sensed, the BatteryCheck() function is called to take the battery voltage every hour. Bike sensing and data collection takes precedence 
 * over battery checking, a notion which is reflected in the order of events in the flow chart. 
 * 
 * The sensor should collect bike data every 0.05 seconds through the Timer1 interrupt. 
 */

void loop() {

 
  // start timing for an hour to pass to collect battery data again
  unsigned long currentBatteryMillis = millis();
  
  // collect the solar data [this is a work in progress, still need to figure out solar stuff]
  float solarVoltageReading = analogRead(solarVoltagePin) * dividerRatio * refVoltage / 1024;

  float solarCurrentReading = analogRead(solarCurrentPin) * refVoltage / 1024;

  // if the voltage reading from the solar panel is high solar intensity
  if (10 < solarVoltageReading && solarVoltageReading <= 16) {
    maxBrightness = highBrightness;
  }

  // if the voltage reading from the solar panel is low solar intensity
  else if (0 <= solarVoltageReading && solarVoltageReading <= 7) {
    maxBrightness = lowBrightness;
  }

  // hold copies of totalSensorValue and sensorCount from ISR
  int totalSensorValueCopy;
  int sensorCountCopy;

  // to read a variable which the interrupt code writes, need to temporarily disable interrupts so the variable doesn't change while we're reading
  noInterrupts();
  sensorCountCopy = sensorCount;
  totalSensorValueCopy = totalSensorValue;
  interrupts();

  // if the number of times ISR triggered equals a set value (adjusted for sensitivity with testing)
  if (sensorCountCopy == maxTrigger) {

    // reset sensorCount and totalSensorValue
    sensorCount = 0;
    totalSensorValue = 0;

    // get the average sensor value
    avgSensorValue = totalSensorValueCopy / maxTrigger;

    // check if average sensor value is within certain threshold (234 mV - 488 mV)
    if (avgSensorValue >= low_threshold && avgSensorValue < high_threshold) {

      // increment the bike counter if a bike is detected
      bikeCounter++;

      // update SD card with bike count
      WriteSD(bikeFile, bikeCounter);
      
      // if a bike is detected, we set a flag to HIGH
      bikeFlag = HIGH;
  
      // every time a new bike is detected, we reset the amount of times we blink the driver alert LEDs
      blinkCounter = 0;  
      
    }
    
  }

  // if a bike is detected and we have not reached maximum amount of times we want to blink for 3 minutes yet
  if (bikeFlag == HIGH && blinkCounter <= maxBlinkCount) {

    BlinkDriverAlert(blinkInterval, maxBrightness);
    
  }

  // if we've surpassed the maximum amount of times we want to blink the driver alert LEDs
  else if (blinkCounter > maxBlinkCount) {

    // turn off the driver alert LEDs
    analogWrite(driverLED, minBrightness);

    // reset the blink counter
    blinkCounter = 0;

    // reset the bike flag 
    bikeFlag = LOW;
    
  }

  // collecting bike sensor data takes precedence over collecting battery data, so we only do so if blinking is over and no bike is detected and an hour has passed
  else if (currentBatteryMillis - previousBatteryMillis >= batteryInterval && bikeFlag == LOW) {
    
    // get battery voltage and set the blinkInterval based on the battery voltage level
    blinkInterval = BatteryCheck();

    // update the previous ms to the current ms for timing the battery
    previousBatteryMillis = currentBatteryMillis;
    
  }

}


/* The BlinkDriverAlert() function takes inspiration from Arduino's BlinkWithoutDelay() example, in which an LED is blinked without using the function delay(). 
 * Because delay() is a blocking function that pauses the program for a given amount of time in ms, we do not want to use it. This function sets the pace at 
 * which the driver alert LEDs blink, given a blinkInterval and how bright we want our LEDs to be, given the maxBrightness. 
 * 
 * Arguments: blinkInterval = how many times the driver alert LEDs blink in 3 minutes (one driver alert cycle); this will need to be calculated
 *            maxBrightness = the maximum brightness of the LED, this is a 8-bit PWM value with a duty cycle from 0-255 and is determined by whether 
 *                            the solar panel detects high or low solar intensity
 *                            
 * Returns: None
 */
 
void BlinkDriverAlert(int blinkInterval, int maxBrightness) {

  // start timing for interval between blinks
  unsigned long currentBlinkMillis = millis();

  // if the time passed is greater than the amount of time elapsed between switching from max and min brightness
  if (currentBlinkMillis - previousBlinkMillis >= blinkInterval) {

    // update the previous ms between blinking
    previousBlinkMillis = currentBlinkMillis;

    // if the LED is at its minimum brightness, we change it to its maximum brightness
    if (ledState == minBrightness) {
      
      ledState = maxBrightness;
    
    }

    // if the LED is at its maximum brightness, we change it to its minimum brightness
    else if (ledState == maxBrightness) {
      
      ledState = minBrightness;
    
    }

    // set the driver alert LED to whatever the led state is 
    analogWrite(driverLED, ledState);

    // increment the number of times the driver alert LEDs blinked
    blinkCounter++;
  
  }
  
}


/* The BatteryCheck() function collects the digital battery reading from the voltagePin, which is the output of the voltage divider that brings down the voltage 
 * from the battery to less than 5 V to be read by the ADC on the Ardiuno, since each pin only takes a max of 5 V. This method of monitoring the battery level 
 * is still a work in progress, since it assumes that battery capacity is linearly correlated to its voltage, which is wrong. 
 * 
 * It then converts this digital value into a voltage value using the equation: voltageReading = batteryReading * (referenceVoltage / 1024) * dividerRatio
 * 
 * By checking the voltageReading, we can determine whether the blinkInterval is high (60 blinks/min) or low (50 blinks/min) and then write the battery data 
 * to the SD card and return the blinkInterval.
 *  
 * Arguments: None
 * 
 * Returns: blinkInterval = how many times driver alert LEDs blink in 3 minutes (one driver alert cycle); this will need to be calculated
 * 
 * Library: Arduino Low Power library
 */

int BatteryCheck() {
  
  // collect the battery data
  int batteryReading = analogRead(voltagePin);

  // convert the digital battery data to analog voltage level
  voltageReading = batteryReading * dividerRatio * refVoltage / 1024.00;

  // if 50% total battery voltage < voltageReading <= 55% total battery voltage; low power
  if (12.06 < voltageReading <= 12.15) {
    
    blinkInterval = lowPowerBlink;
  
  } 

  // if voltageReading > 55% total battery voltage; high power
  else if (voltageReading > 12.15) {
    
    blinkInterval = highPowerBlink;
  
  }

  // if voltageReading <= 50% total battery voltage; low power
  else if (voltageReading <= 12.06) {
    
    // this puts Arduino into low power mode, shutting down ADC and BOD for 8 seconds each loop (may want to implement this myself instead of using library)
    LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    
    blinkInterval = lowPowerBlink;
  
  }

  // write battery data to SD card
  WriteSD(batteryFile, voltageReading); 

  // return whether we want to do 50 blinks/min or 60 blinks/min depending on voltage reading
  return blinkInterval;
  
}


/* The WriteSD() function uses SPI communication and the Arduino SD library to open a given filename and write data to that file if the file was successfully opened. 
 * If a file was not opened successfully, we will need to remove the SD card to check if it is full (it will most likely take a while to fill the SD card, as we have 
 * 8 GB of space). This check can be done during our monthly maintenence checks on the pneumatic tube. 
 * 
 * Arguments: filename = name of the file we want to write to, either bikeData or batteryData
 *            data = data to be written to the file of either how many bikes were counted (bikeCount) or battery level (voltageReading)
 *            
 * Returns: None 
 * 
 * Library: Arduino SD library, Arduino SPI library
 */

void WriteSD(const char *filename, float data) {
  
  myFile = SD.open(filename, FILE_WRITE); 
  
  if (myFile) {
    
    myFile.println(data);
    myFile.close();
    
  }

  else {
    
    BlinkErrorLED();
    
  }

}


/* BlinkErrorLED() functions very similarly to the BlinkDriverAlert() function, but instead of blinking for a certain amount of times, it blinks an 
 * error LED (separate from the driver alert LEDs) indefinitely until the system is checked on and rebooted. 
 * 
 * Arguments: None
 *                            
 * Returns: None
 */
 
void BlinkErrorLED() {
  
  unsigned long currentErrorMillis = millis();
  
  if (currentErrorMillis - previousErrorMillis >= errorInterval) {
    
    previousErrorMillis = currentErrorMillis;
    
    if (errorLEDState == LOW) {
      
      errorLEDState = HIGH;
    
    }
    
    else if (errorLEDState = HIGH) {
      
      errorLEDState == LOW;
    
    }
    
    analogWrite(errorPin, errorLEDState);
  
  }
  
}
