// sensor code values
int sensorPin = A0;        
int sensorValue = 0;  
float averageValue = 0;
int bikeCount = 0;
int ledPin = 9;

// these are values taken from 11 measurements using first code block
int high_threshold = 150;
int low_threshold = 41;

// initial code used to take threshold values, will need editing later
void setup() {
  
  Serial.begin(9600);
  pinMode(ledPin, OUTPUT);  
  digitalWrite(sensorPin, HIGH); 
  
}

void loop() {
  
  // read the value from the sensor
  sensorValue = analogRead(sensorPin);  

  // get the ambient pressure when Arduino first starts up
  if (millis() < 2000) {
      averageValue += ((float)sensorValue-averageValue)/10.f; // why are we dividing by 10?  
  }
  else {  
    if (sensorValue > averageValue + 4) { // what is the +4 for?
      bikeCount++;
      
      Serial.print("bike count: ");
      Serial.println(bikeCount);
      Serial.print("sensor value: ");
      Serial.println(sensorValue);
      Serial.print("average value: ");
      Serial.println(averageValue);
      
      averageValue = sensorValue + 10; // why are we adding 10?

      digitalWrite(ledPin, HIGH);
      delay(1000);
    }
    else {
    
        digitalWrite(ledPin, LOW);
        averageValue += ((float)sensorValue-averageValue)/100.f; // what are we dividing by 100?
    }   
  }
  
}


//// different method: have two thresholds with hysteresis, so that you increment the count when the 
//// high threshold is exceeded and do not increment the count further until the level falls below the lower threshold
//void setup() {
//  active = false;
//}
//
//void loop() {
//  sensorValue = analogRead(sensorPin);
//  if (sensorValue > high_threshold && !active) {
//    bikeCount++;
//    active = true;
//  }
//  if (sensorValue < low_threshold) {
//    active = false;
//  }
//}
