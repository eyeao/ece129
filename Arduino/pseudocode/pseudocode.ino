#include <SD.h>
#include <SPI.h>

File myFile;

// sensor values
int sensorPin = A0; // analog input pin for sensor to be read by 10-bit ADC 
int sensorValue = 0; // actual digital value of sensor input 
int bikeCounter = 0; // counter to keep track of number of bikes that passed
int bikeFlag = LOW; // flag to keep track of whether a bike is detected or not

// these are values taken from 11 bike measurements Gina took
int high_threshold = 150;
int low_threshold = 41;

// voltage reading from solar
int solarPin = A2;

// values for driver LED alert
int driverLED = 9; // digital output pin for the 100 LED driver alert, has PWM
//int maxBrightness = 0; 
//int minBrightness = 0;
//int highBrightness = 255; // this is a PWM duty cycle value, may need to change
//int lowBrightness = 127; // just an estimate rn 
int blinkInterval = 1000; // to be changed, how many blinks the LED does in 3 min, one driver alert cycle 
//int ledState = maxBrightness;
int blinkCount = 0;

// for testing (delete later):
int maxBrightness = 255;
int minBrightness = 0;
int ledState = maxBrightness;

// battery values
int voltagePin = A1;
float dividerRatio = 3.2; // this is (R1+R2)/R2 where R1 = 22kOhm and R2 = 10kOhm 
float refVoltage = 5.00; // this is what we want to bring voltage down to

// keep track of amount of time passed for battery
unsigned long previousBatteryMillis = 0;
//const unsigned long batteryInterval = 3600000UL; // 1 hour
const unsigned long batteryInterval = 4000;

// error pin
int errorPin = 8;

void setup() {
  
  // initialize serial data transmission
  Serial.begin(9600);
  
  // set the error LED and driver alert LED pins to OUTPUT
  pinMode(errorPin, OUTPUT);
  pinMode(driverLED, OUTPUT);

//  // initialize SD card
//  if (SD.begin()) {
//    Serial.println("SD card initialization successful.");
//  } 
//  else {
//    Serial.println("SD card initialization failed.");
//    return;
//  }

}

void loop() {

  // start timing for an hour to pass to collect data again
  unsigned long currentBatteryMillis = millis();
  
  // collect the sensor data
  sensorValue = analogRead(A0);
  
  // if a bike is detected
//  if (low_threshold <= sensorValue <= high_threshold) {
   if (sensorValue > 50) {
     Serial.println("sensor value: ");
     Serial.println(sensorValue);
     bikeCounter++;
     bikeFlag = HIGH;
     delay(1000); // this is so we don't sense the back tire
     
//    [RF]
//
//    // write data to SD card
//    writeSD(bikeData, bikeCounter);
//
//    // check if it's high light
//    if (solarPin >= [solar threshold]) {
//      maxBrightness = highBrightness;
//    }
//
//    // check if it's low light
//    else if (solarPin < [solar threshold]) {
//      maxBrightness = lowBrightness;
//    }

    // start blinking driver alert
    if (bikeFlag == HIGH) {

      bikeFlag = LOW;

      while (blinkCount < blinkInterval) {

         if (ledState == minBrightness) {
            ledState = maxBrightness;
         }

         else if (ledState == maxBrightness) {
            ledState = minBrightness;
         }

         analogWrite(driverLED, ledState);
         blinkCount++; 
         Serial.println("blink count: ");
         Serial.print(blinkCount);
      }

      if (blinkCount >= blinkInterval) {

        // turn off driver alert
        Serial.println("driver alert off");
        ledState = minBrightness;
        blinkCount = 0; 
        bikeFlag = LOW;
     
      }
    }
    
  }
  // if time passed greater than or equal to an hour and no bike sensed (this may not be very accurate, but we don't need too much accuracy for periodically checking battery data)
  else if (currentBatteryMillis - previousBatteryMillis >= batteryInterval && bikeFlag == LOW) {
    // update the previous time to the current time so we can continuously track hour intervals from when Arduino is powered
//    blinkInterval = BatteryCheck();
    Serial.println("get voltage of battery");
    Serial.println(currentBatteryMillis);
    previousBatteryMillis = currentBatteryMillis;
  }

}
   

//int BatteryCheck() {
//  
//  // collect the battery data
//  int batteryReading = analogRead(voltagePin);
//
//  // convert the battery data to voltage level (refVoltage/1024 = for every 4.88 millivolts of the voltage value of on Arduino analog pin, the digital value will be incremented by one and so on for every multiple voltage of 4.88mV, the respective digital value will be produced.)
//  voltageReading = (batteryReading * (refVoltage / 1024.00) * dividerRatio;
//
//  
//  
//  if ([50% total battery voltage] < voltageReading <= [55% total battery voltage]) {
//    [blink red error LED and send error message];
//    [charge from solar panel to 60% of total battery voltage];
//    blinkInterval = lowPowerBlink;
//  } 
//  
//  else if (voltageReading > [55% total battery voltage]) {
//    blinkInterval = highPowerBlink;
//  }
//  
//  else if (voltageReading <= [50% total battery voltage]) {
//    [put Arduino into low power mode using SLEEP_MODE_PWR_DOWN];
//    [charge from solar panel to 60% total battery voltage];
//    [manual reboot, push reset button];
//  }
//
//  writeSD(batteryData, voltageReading); // call function to write battery data to SD card
//
//  }
//  return blinkInterval;
//  
//}
//
//void writeSD(fileName, data) {
//  
//  myFile = SD.open("fileName.txt", FILE_WRITE); // what file type do we want to store data in?
//  if (myFile) {
//    Serial.println("Writing to file...");
//
//    // write to file
//    myFile.println(data);
//    myFile.close();
//    Serial.println("Done writing to file.");
//  }
//  
//  else {
//    Serial.println("Error opening fileName.txt");
//  }
//
//}
